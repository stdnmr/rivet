BEGIN PLOT /BABAR_2007_I722622/d03-x01-y01
Title=Spectrum for $\Xi_c^{\prime+}$ production
XLabel=$p$ [GeV]
YLabel=$1/N\text{d}N/\text{d}p$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2007_I722622/d03-x01-y02
Title=Spectrum for $\Xi_c^{\prime0}$ production
XLabel=$p$ [GeV]
YLabel=$1/N\text{d}N/\text{d}p$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2007_I722622/d04-x01-y01
Title=Helicity angle for $\Xi_c^{\prime0,+}$ decay
XLabel=$\cos\theta$
YLabel=$1/N\text{d}N/\text{d}\cos\theta$
LogY=0
END PLOT