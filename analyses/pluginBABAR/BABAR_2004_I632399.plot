BEGIN PLOT /BABAR_2004_I632399/d01-x01-y01
Title=Branching Ratio $B\to\phi X$
YLabel=$\text{Br}(B\to\phi X)$ [\%]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2004_I632399/d02-x01-y01
Title=Momentum Spectrum for $\phi$
YLabel=$1/N_B\text{d}n/\text{d}p$ [$\text{GeV}^{-1}$]
XLabel=$p$ [GeV]
LogY=0
END PLOT
